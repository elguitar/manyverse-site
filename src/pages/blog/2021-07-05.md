---
path: '/blog/2021-07-update'
date: '2021-07-05'
title: 'July 2021 update'
author: 'André Staltz'
authorUrl: 'https://staltz.com'
---

Hey backers!

It's the 5th day of the month again, and here's a new update on what's happening in the Manyverse project!

We have a new release coming out today, with a focus on fixing bugs and crashes that were reported by users, but there are two new features shipped as well. **Version 0.2107.5** contains:

* 🎉  Feature: auto-save draft posts and replies
* 🎉  Feature: Friend requests in the Activity tab
* ✅  (iOS) Bug fix: height of the header on new iPhones
* ✅  Bug fix: should not crash when fetching profile name
* ✅  Bug fix: should not crash when rare user has no ID
* ✅  Bug fix: disallow taps on follower count when zero
* ✅  Bug fix: connections tab should not be chaotic
* ✅  Bug fix: disconnect and forget should not reconnect
* 🔷  Drop support for P2P invites
* 🔷  Improve replication performance
* 🔷  Improve Portuguese (Brazilian) translation

iOS users will appreciate the fixed header size on new iPhone devices. This was possible thanks to [Browserstack](https://browserstack.com/) which provides us a free plan as an open source project. For deeply technical reasons, we can't test the app on iPhone simulators, we need to use real devices. Getting access to all these physical devices is expensive. However, Browserstack's App Live product allowed us to [test the app on several iPhone devices on the cloud](https://gitlab.com/staltz/manyverse/-/issues/1347#note_618526686), and confirm that the layout looks good on all of them.

We also got crash reports from Android users, about a [crash specific to Android 11](https://gitlab.com/staltz/manyverse/-/issues/1411). I think [Google released a breaking change](https://developer.android.com/training/articles/user-data-ids#mac-11-plus) that challenges our app architecture. We can't query for the network interfaces on the device, which we need for local sync over Wi-Fi. We're investigating how to best fix this issue, but I wanted to let you all know that it's specific to Android 11.

## New features

The new release now **automatically saves your draft posts and replies**. There is no manual confirmation on your part, just type your posts and Manyverse will automatically save the text every 1 second! This was important in those cases when the app crashed and you lost your draft posts, which is a big deal. Worry no more. :)

We are also introducing a new concept to SSB, which we call **friend requests**. From this version onwards, strangers cannot connect to you, unless you approve them. By "stranger", we mean any account that is not a friend nor a friend of a friend (i.e. your configured hops range). When they try to connect with you, they will show up on the Activity tab as a friend request. Tap the friend request to open their profile and press "Follow", which should now allow them to connect with you. This feature was an important piece for community safety, and was actually planned during the [UX Research](https://www.manyver.se/ux-research/) we had from Wouter Moraal. Room aliases actually make your account available for the whole world, so we needed friend requests to counter some vulnerabilities, and keep the social graph grounded in trustful relationships.

## Connections overhaul

A hidden development this month was the full rewrite of the replication logic and connection to friends, which took most of my time. As part of my work on [NGI Pointer](https://github.com/ssb-ngi-pointer), I rebuilt and thoroughly tested core components in SSB: [ssb-ebt](https://github.com/ssbc/ssb-ebt), [ssb-friends](https://github.com/ssbc/ssb-friends), [ssb-replication-scheduler](https://github.com/ssb-ngi-pointer/ssb-replication-scheduler). I also wrote a new ["firewall" module](https://github.com/staltz/ssb-conn-firewall/), which is used to create the friend requests feature.

Replication in Manyverse should now be quicker and more efficient. We also dropped support for P2P invites (also known as DHT invites), because they rarely worked and we were getting many bug reports about that. Room aliases should now be a proper replacement, they work more reliably and are more user friendly as a short addresses.

The UI part of connectivity was also improved. The Connections tab was infamous for updating chaotically, and in this release behaves more neatly and less frequently. Other small aspects of managing the connections list were also improved.

## Desktop project

As of today, Jacob and I have received funding from the SSB community (via the so-called Handshake Council and [ACCESS](https://opencollective.com/access)) to deliver Manyverse on desktop environments! We have received funding equivalent to 100h each, and today the two of us got on a call to discuss the development challenges. Shall we need more time, we are fortunate to have funding from all you backers on OpenCollective. We have good hopes that in a couple of months we will deliver the app on macOS, Linux, and Windows!

Kind regards,

— @andrestaltz
