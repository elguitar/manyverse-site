---
path: '/blog/2020-01-update'
date: '2020-01-05'
title: 'Jan 2020 update'
author: 'André Staltz'
authorUrl: 'https://staltz.com'
---

Hey backers! I'm happy to open 2020 with great news. In December, I worked 100 hours, of which 26 hours were funded by you, a total of 80 backers. Guess what I worked on? **Bringing Manyverse to iOS.**

![A graphic design where there is an iPhone 11 Pro Max on the right, displaying the newly released Manyverse app for iOS, and text on the left; The text says "Manyverse for iOS" and a black badge saying "Download on the App Store"](../../images/ios-launch.png)

This is it! Many of you are iPhone users who have long wanted this project to be available on your phones, now you can finally have it. It's [available now on the App Store](https://apps.apple.com/app/manyverse/id1492321617)! As backers of this project, you are the first ones to read about these news. ;)

The iOS app is fully functional, and mirrors almost all functionality from the Android app. The only missing feature is Bluetooth sync, but if you want to sync locally with a friend, a shared Wi-Fi network is sufficient for that. We will try to bring Bluetooth support to iOS later, but there is a chance it might not work between Android and iOS. Otherwise, all other SSB features should work the same way on both platforms, that's also the goal for future features to be released.

One important thing that you need to be aware of is that **initial data sync will likely be very slow on iOS**, it could heat up your device, and it could take even several hours to complete! This is (unfortunately) normal, so if it happens to you, just have patience and it will complete within some hours. After having synchronized most data, your user experience will be smooth and usable from that point onwards.

On either of the two mobile platforms, there is no easy way to solve initial sync, but on iOS it's a bit worse because the app cannot always process data in the background. That said, I think it's soon time to *properly solve* initial sync, and it's a problem I want to dedicate myself to fix. It's solvable but it's a lot of work: we could rewrite or improve the replication algorithms, we could build prioritization so that the CPU can handle UI interactions at all times instead of doing only data processing, we could adapt and utilize the amazing Rust and SQLite tech stack that [Sunrise Choir](https://opencollective.com/sunrise-choir) has built, among other things. As you have followed this project for over a year, you know we can deliver it, so keep in mind that the current state of the app is temporary. Manyverse is still beta-quality software.

To bring iOS support to you, it was a marathon of bug fixes, because the app wouldn't compile in Xcode before hundreds of changes were made and tweaked to the precise settings. [See this issue](https://gitlab.com/staltz/manyverse/issues/680) where I list all the subtasks and their completion dates. When it finally ran on my iPad, I literally shouted of joy. I can't remember what I shouted. Afterwards, I worked on a lot of style customizations to fit the iOS design. It still looks like an Android-first UI design, but respects common iOS guidelines. In the near future I want to tweak the UI a bit more, and then it might feel even better on iOS. If you take a careful look, Manyverse also has a new app icon, with bolder lines. (It's coming to Android too, soon.)

One final bit of news is that I believe *I'll have to work a bit less on Manyverse* for the next few months. The chart below shows how many hours we (me and Jacob) have worked, and how many of those hours were funded by this OpenCollective. (The period between 2019-06 and 2019-09 corresponds to the time when [ACCESS sent us 8400€](https://opencollective.com/access).)

![A two-dimensional chart where x-axis is a timeline where each tick is a month, and y-axis is hours worked. The number of hours per month fluctuates between 20 and 160, and a fraction of those hours each month is marked as funded, sometimes 100%, but most of the time under 50%](../../images/hours-worked-funded.png)

Most of those hours were funded from my own savings, I was fortunate that my side business was doing well to support this. But now my savings are smaller and I need to go back to money making. This is not about motivation to work on Manyverse, it's about survivability. So for the following months, I'll try to work roughly the amount of hours that you fund and maintain a balance. For instance, if the OpenCollective pays for approximately 26 hours of work, then I'll work no more than 30 hours. As soon as my savings are large enough, I can go back to working in a *more-than-funded* mode. Of course, the possibility of grants from other sources could also change this situation suddenly. I also hope that with the arrival of an iOS app, a new wave of backers could join this Open Collective!

But expect progress! I'm close to implementing private messaging, which is the next issue in the roadmap. And afterwards there are other low-hanging fruits too. See you on Scuttlebutt, and I hope you enjoy the new app!

— @andrestaltz