---
path: '/blog/2019-11-update'
date: '2019-11-05'
title: 'Nov 2019 update'
author: 'André Staltz'
authorUrl: 'https://staltz.com'
---

Hey backers, I'm glad to be reporting back to you! Manyverse is progressing just as planned, the roadmap getting smaller as we ship new features. Last month, I worked 144 hours, out of which 26 hours were funded by you. This Open Collective now has 80 backers, a steady increase over the months. Here's what we shipped:

- **Feature: backup/restore your account** [v0.1910.31](https://gitlab.com/staltz/manyverse/blob/master/CHANGELOG.md#0191031-beta)
- **Feature: welcome screen with tutorials** [v0.1910.31](https://gitlab.com/staltz/manyverse/blob/master/CHANGELOG.md#0191031-beta)
- **Improve responsiveness and speed of the UI** [v0.1910.15](https://gitlab.com/staltz/manyverse/blob/master/CHANGELOG.md#0191015-beta)

![A graphic design where there is a phone on the right, displaying the Manyverse app open, and text on the left; The text says "Welcome screen + tutorial", and the phone has Manyverse open with the Welcome screen showing two butterflies as an artwork and a text that says "Welcome to Manyverse! Social networking can be simple, neutral, non-commercial, and built on trust between friends. This is what Manyverse stands for, and we hope you too can make it your digital home. All this is made possible by the novel SSB protocol"](../../images/welcome-screen.png)

**The new Welcome screen** provides a tour through some Manyverse essentials. So far, the app has had no help screen or tutorials, hoping that the UI would be intuitive enough, but in reality some aspects of the SSB protocol are so unique that they deserve some introduction. Some users have also told me that it's worth informing, for instance, about the current impossibility to delete content. This screen will appear only the first time you open Manyverse with an empty account.

Alongside the Welcome screen, there is also a [new FAQ page](https://manyver.se/faq) on our website that goes into even more details. We hope to cover all the repeatable questions we get about the app, adding content as we go. Feel free to send us a few questions. :)

![A graphic design where there is a phone on the right, displaying the Manyverse app open, and text on the left; The text says "Backup & Restore; with a 48-word recovery phrase", and the phone has Manyverse open with the Restore account screen showing a popup that says "Welcome back! Your account's identity is now restore. Now, to restore your data, connect with friends that have interacted with you before, to redownload your data from them."](../../images/backup-restore.png)

The app now supports *exporting* (backing up) your account's identity. On the drawer menu on the left, you can find this under the menu item **Backup**, with a key icon. To **restore your account**, you do that at the end of the Welcome screen. Backup and restoration works through a confidential recovery phrase made of 48 words, much like Bitcoin wallets have seed words for backup and restoration. One of the benefits of this new feature is that you can easily swap between distribution stores (Google Play or F-Droid), as long as you have your identity backed up with the recovery phrase.

This backup, though, only concerns your *identity* (your cryptographic keypair). To back up your data, you need to make sure friends have synced with you, so you can re-download your data from them later when you restore your identity. We wanted to make this even easier, by putting **your SSB data in a public folder on Android**, that you could access through any other app. This would have been great news also for other SSB apps on Android: they could reuse the same folder where your SSB data lives! Unfortunately, Google is going to issue a [breaking change (*Scoped Storage*)](https://www.androidcentral.com/what-scoped-storage-android-q) soon that makes shared folders almost an impossibility. For Manyverse, we're investigating what's the best way to do this for the user experience, that will also comply with Android's new restrictions. In the meanwhile, we'll keep storage how it has been, with the SSB data stored in a folder internal to the Manyverse app, not shared with other apps.

By the way, more news regarding Google: last month I got a pretty scary email from Google Play Store informing that **Manyverse was removed from the Store**. I immediately stopped everything to work on figuring out why, and to make a new compliant release. It turned out that we can't thank backers from within the app, because (I suppose) Google thinks this is a special paid feature, and as so would have to be paid for through their Payments API. This is a real bummer, but gladly the app went back online once I removed the Thanks screen. Actually, the Thanks screen still remains if you download the app from F-Droid or Dat Installer. It's only on Google Play that it doesn't show. More about this incident [here](https://twitter.com/andrestaltz/status/1184727365588520960).

On a bright note, **Manyverse is finally being introduced to people who live off grid**. [Nicolas Pace](https://twitter.com/nicopace) from [Altermundi](http://altermundi.net/) is in frequent contact with indigenous communities around the world, and this time he introduced Manyverse to indigenous youth in Mexico, who often have no internet access. Although the app is currently lacking in localization, they still found it promising and fun to use!

![In a small auditorium that seems to be a tent, there are a dozen young university students holding their phones up in selfie position or using it in normal stance; some are smiling, some are focused on exploring their phones](../../images/mexican-indigenous-use-manyverse.jpg)

And just three days ago, I attended a **meetup for community networks in Brazil**. I finally got to meet Luandro in person (he's collaborated on Manyverse before, and now makes SSB Android apps for specific communities), and met new faces in the Latin American community network space (we were mostly Brazilians and Argentines). In the pictures below, Luandro and I are doing an introduction to SSB, Manyverse, and Patchwork. We had an onboarding party where many got to try out the app, and people were amazed that it just works.

![In a small Brazilian school classroom, there are a dozen people at desks with their computers open while listening to Luandro speaking in front about SSB, Manyverse, and Patchwork](../../images/luandro-retreat.jpeg)

![A selfie with Andre, Luandro, and an accidental person passing by](../../images/andre-luandro-retreat.jpeg)

This was my first contact with real community networks, and I have to say I learned a *lot*, specially about the sociological, economical, and cultural context that leads them to the decision of starting a local network. I wrote a lot more about my experience in that retreat in this SSB thread which you may be able to open on a desktop app: `%djrotK2IOBWxvp2+XldV9x0/65f0psbsy9aIffoPtGc=.sha256`. Getting Manyverse into the hands of community networks is also a sign that this project is maturing from prototype quality to actual deployment.

Thanks for reading and see you next time!

— @andrestaltz