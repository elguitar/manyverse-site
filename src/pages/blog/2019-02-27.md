---
path: "/blog/bluetooth-sync"
date: "2019-02-27"
title: "Bluetooth Sync"
---

Bluetooth Sync has arrived to Manyverse, making it a truly off-grid social network! What does it mean for a mobile app to be off-grid? It means that internet connectivity is useful, but not *required*. Many people in developed countries enjoy reliable and fast internet, wherever they go. But this is not the case for everyone on the planet.

![Smartphone with app Manyverse opened, displaying a green button that says 'Bluetooth seek'](../../images/bt-seek.jpg)

When you have no internet connection, it should be easy to share data from phone to phone, directly, without intermediaries. Computers have had this capability for a very long time, through floppy disks and USB drives. Phones should not be an exception, given that they are also computers. Indeed, this used to be the case back in the days of feature phones: sharing pictures or music via Bluetooth was common.

With the advent of cloud-based mobile apps, data has been locked up in data centers, and "shared" only between data centers, depriving users from the option of sharing their data in whatever way they prefer. With Manyverse, we want to change that. Data should be first on your device, and you should be able to share it with whoever you want, without intermediaries, in whatever ways your device allows you to share data.

**Bluetooth Sync is now available on Manyverse** version [0.1902.27-beta](https://gitlab.com/staltz/manyverse/blob/master/CHANGELOG.md#0190227-beta) onwards. This means one phone can fetch the latests posts and comments directly from another phone nearby (and vice-versa). To use it, follow these steps on the two devices:

1. Turn on Bluetooth in Android settings
2. Open Manyverse in the "Connections" tab
3. Press the green button and choose "Bluetooth seek"
4. Wait until the other phone appears on the list in the Connections tab
5. Select the other phone in the list to connect to it
6. The latest data will be downloaded from the other phone

![Two smartphones placed side by side, the smartphone on the right shows a list of posts, the smartphone on the left shows a 'synchronize' button with the name of the smartphone on the right](../../images/bt-two-phones.jpg)

Synchronization is often fast enough to share a yearlong diary in less than 5 minutes. Smaller databases sync in less than 30 seconds. Give it a try and see for yourself how simple the process is.

This is just one development to bring us closer to off-grid readiness. Our goal is to make it possible for communities in the [Global South](https://en.wikipedia.org/wiki/Global_South) to utilize Manyverse off-grid features. When friends can post content offline at their homes, and then regularly meet up to update their Manyverse databases with the latest news, and make this a habit, then we will know we have made it possible to enjoy digital communications without forcing the requirement of unaffordable data plans.

You can help us by [tweeting](https://twitter.com/intent/tweet?original_referer=https%3A%2F%2Fmanyver.se%2Fblog%2Fbluetooth-sync&text=Bluetooth%20Sync%20has%20arrived%20to%20Manyverse&tw_p=tweetbutton&url=https%3A%2F%2Fmanyver.se%2Fblog%2Fbluetooth-sync&via=manyver_se) this announcement or [donating](/donate) to our project. Thank you!