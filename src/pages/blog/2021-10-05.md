---
path: '/blog/2021-10-update'
date: '2021-10-05'
title: 'October 2021 update'
author: 'André Staltz'
authorUrl: 'https://staltz.com'
---

Exactly **three years ago** we launched Manyverse and began this Open Collective. Today is a special day, and I prepared a retrospective video to celebrate it! Huge thanks to all our backers, some of you have been with us since day 1 on OpenCollective and are still with us! 👏

Before we look at the 3-year retrospective, we have a new version coming out on Play Store, App Store, and APK download today. Here are the new goodies in **version 0.2110.5**:

* 🎉  Redesigned Compose Message screen
* ✅  Bug fix: crash related to database RangeError
* ✅  (iOS) Bug fix: record and play audio messages
* 🔷  (Android) Reduce APK size by 71MB
* 🔷  (Android) Support Android 5.0
* 🔷  Replace the Connections slide-in menu with a dialog
* 🔷  Improve translations in Arabic, Chinese Traditional, German, Korean, Ukrainian

The shiny feature this month is a brand new **Compose screen**:

![A graphic design where there is a phone on the right displaying the Manyverse app showing the improved Compose Screen where the layout has been repositioned and a new menu for mentions is shown; Text to the left of the graphic design says "Compose screen redesigned; Preview before Publish"](../../images/compose-redesigned.png)

When writing a new message, the Compose screen is now organized vertically as if it were a published message. The "Preview" button is more obvious than before, and it's not possible to mispress the "Publish" button. This also brings Manyverse closer to the Patchwork user experience, where all posts are previewed before published, which is smart given that you cannot delete them afterwards. Also, mentioning people with `@` is visually cleaner and less buggy than before. I hope you all like it! That said, we still have some user feedback about this screen that will be worked on in the coming weeks. ;)

**APK size and Android 5.0 support.** Last month we did some big upgrades to the tooling that builds the app, and this had some adverse effects such as a much larger Android APK size. The size has now been dropped to 41 MB. Also, as promised in the previous newsletter, support for Android 5.0 has been restored! This has been quite important for Manyverse users in Myanmar.

**Manyverse iOS** has been missing support for audio messages, because technically audio file playback worked very different than on Android. I ended up rewriting the underlying library, [ssb-serve-blobs](https://github.com/ssbc/ssb-serve-blobs), with iOS in mind. Now you all can record (from the Compose screen) and playback audio messages.

**Translations** were improved for several languages, thanks to community contributors! There are now 10 languages in Manyverse that are 80% complete.

## Retrospective

![An illustration of the night sky at dawn with some text overlaid: "Manyverse is 3 years old"](../../images/3yo.png)

I put a [short video on Twitter](https://twitter.com/manyver_se/status/1445424124361420803) revising all these 3 years of improvement. Check it out!

Manyverse was the first ever SSB app on mobile, an incredible feat considering that it's Node.js on mobile, and that it's full peer-to-peer on limited devices. The beginning was a real struggle because it barely worked, and was extremely slow.

We've improved a lot over the years, and I have some statistics to share with you. I measured these by installing Manyverse from 3 years ago, running it as my main app, and then comparing it with today's version. Both versions had the same amount of social content.

**App startup is 2.5x faster**

- 3 years ago it would take 18 seconds
- Today it takes 7 seconds

**Initial sync is 13x quicker**

- 3 years ago it would take 12 hours
- Today it takes 55 minutes

(I am not satisfied with 55 minutes! Once we have partial replication built into Manyverse, I hope initial sync to drop down to one minute)

**Database occupies 2x less space** (not including blobs)

- 3 years ago it used 2.0 GB
- Today it uses 950 MB

Not only is the app faster and lighter, it has dozens of more features than before:

* Bluetooth sync
* Room connections
* Attach pictures
* Emoji reactions
* Account backup
* Mentions
* iOS support
* Localization
* Nested threads
* Settings
* Unread markers
* Audio messages
* Private messaging
* Room aliases
* Full-text search
* Friend requests
* Activity tab
* (Lots more)

All this, and I still think the app is in beta. Three years is too much time to be in beta, but I think we're soon out of beta. I think the critical parts to become production ready are: partial replication, desktop support, and guiding users during their first use of the app. That's not a large list. I'll be working full-time on Manyverse starting this month, so the list is going to shrink! :)

I want to thank everyone who helped make all this possible. Bug reports and feedback were super appreciated. Financial support has been vital! Translations have been amazing. Thank you also for the emotional support, the App Store reviews, and advocacy on the internet. Manyverse has some history but its future is what I'm really looking forward to.

PS: we were spontaneously [featured on HackerNews this month](https://news.ycombinator.com/item?id=28607995).

Kind regards,

— @andrestaltz
