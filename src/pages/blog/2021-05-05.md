---
path: '/blog/2021-05-update'
date: '2021-05-05'
title: 'May 2021 update'
author: 'André Staltz'
authorUrl: 'https://staltz.com'
---

Hey backers,

I'm super grateful for all of you, all 137 of you, because you create financial stability for this project. Last month, one of you donated an incredible amount, that *doubled* the monthly income Manyverse receives. I was shocked, I could barely believe it! This brings us closer (63%) to the goal of supporting one developer full-time. Jacob and I already began planning how to allocate our budget for the coming months, excited that we are able to pour more work into this. Although heavy-weight donors are fabulous news, *every* donor matters, because in aggregate it creates more stability when the income fluctuates less. You matter!

## New release

Last month I focused on fixing some critical bugs and crashes, and bringing one new UI feature to you all. **Version 0.2105.5** is rolling out today on Google Play (or indie releases on [manyver.se/apk](https://manyver.se/apk)) and App Store. The new changes are:

* 🎉 Feature: profile screen shows follower count
* 🎉 Feature: support some SSB URIs
* 🔷 Improve stability of peer connections
* ✅ Bug fix: reset and fix indexes that were fractured
* ✅ Bug fix: profile screen no longer has bio glitches
* ✅ Text on the Welcome screen no longer overflows

## Follower count

![A graphic design where there is a phone on the right displaying the Manyverse app showing the improved Profile Screen where Zenna's information are displayed, such as biography, follower count, following count, and others; Text to the left of the graphic design says "Better Profiles; Shows follower count, shows SSB ID"](../../images/announcement-profile-follower-count.png)

This new version introduces a basic feature that has been missing in Manyverse, in comparison to others like Patchwork. On every profile, you can now see the number of persons following that profile, and the number of person that profile follows. If you tap those numbers, it opens a screen where you can scroll through those persons. We also put the SSB Identifier near the profile's name, so you can make sure who are you interacting with, cryptographically. This new interface also fixes a visual glitch with the bio text rendering incorrectly or overflowing.

Considerable thought was put into this UI design, and special thanks goes to David Gómez who provided lots of UI design feedback and Figma sketches. One of the things we discussed in length, including with Jacob Karlsson, was whether or not to display a counter of blocks. We experimented with it in a few different ways, and decided to not include block counts this time. We came to the conclusion that seeing information on who blocks who can create negative feelings in the community, and we noticed that other social apps don't display this information either – probably intentionally. We want to deliberate on a design for displaying blocking information, putting it in context and maybe even explaining it in a tutorial. It's not a matter of just showing another number. So we updated our feature roadmap to come back to this issue in the future.

## Demo of rooms 2.0

Last month I said we're working hard to complete the [implementation](https://github.com/ssb-ngi-pointer/go-ssb-room) of [rooms 2.0](https://github.com/ssb-ngi-pointer/rooms2). I'm happy to say it is completed, and the first room 2.0 now in production can be visited at [hermies.club](https://hermies.club). It already has 17 members and it's already supported in the newly released Manyverse version. To become a member, please contact me or `@cryptix` and if we know each other, we'll add you! This room is a members-only server, so we are accepting people on the basis of trust.

But we're not fully ready yet. We want to make a proper announcement of this, and it will come with beautiful videos that explain how it works, and how to use it with Manyverse. So if you're still confused about this new concept, don't worry, soon it will all make sense!

## Extras

If you enjoy watching live streams, last Sunday I worked on the new profile UI and decided to stream it. You can [watch it on YouTube](https://youtu.be/rBShY_OsLYQ). I did this impromptu, and I'm not sure if live streaming is my thing, so I can't promise there will be more in the future, but I hope this one is enjoyable.

Final tip before you go: if you have a MacBook with the M1 chip, Manyverse is available on the App Store. ;)

Kind regards,

— @andrestaltz
