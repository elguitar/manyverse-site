---
path: '/blog/2019-07-update'
date: '2019-07-05'
title: 'Jul 2019 update'
author: 'André Staltz'
authorUrl: 'https://staltz.com'
---

Wonderful backers, thank you for subscribing to this newsletter and financially supporting Manyverse become a great mobile app for Scuttlebutt (SSB). This project has now **67 backers**, 3 more than last month. In June, I worked 68 hours, and now I’m glad to bring to you some long-awaited new features:

![A graphic design where there is a phone on the right, displaying the Manyverse app open, and text on the left; The text says "Connections Redesigned", and the phone is Manyverse with the Connections tab where there are 2 online peers and one suggestion for a local-area network peer connection](../../images/201907phone.png)

The Connections tab has been completely redesigned in [version 0.1907.5-beta](https://gitlab.com/staltz/manyverse/blob/master/CHANGELOG.md#019075-beta). This may seem like visual improvement only, but in fact it demanded a complete rework of underlying modules deep inside the SSB stack. In the SSB community, we have talked about the “gossip refactor” for ages, and now that task is done. [SSB CONN](https://github.com/staltz/ssb-conn) combines [ConnDB](https://github.com/staltz/ssb-conn-db), [ConnHub](https://github.com/staltz/ssb-conn-hub), [ConnStaging](https://github.com/staltz/ssb-conn-staging), and [ConnQuery](https://github.com/staltz/ssb-conn-query) to allow SSB developers to have full control over all types of peer-to-peer connections, in an organized and predictable way.

The CONN stack introduces the concept of **staging**, which is a registry of suggestions of peer connections, allowing the user to decide which peers to connect to. In Manyverse, staged connections appear in light blue below real connections. This and other other CONN features enabled some important fixes and new features in the app:

- Yellow dot indicates connections being attempted
- Does not connect to blocked peers
- Does not automatically connect to LAN peers (good for privacy), unless they are friends
- Bluetooth sync is easier to follow what is going on
- User can choose to disconnect from any peer

Developing SSB CONN was also a pre-requisite to building the promised “new kind of server” for SSB, which I also worked on last month. That’s coming along nicely. You may have also tried to use the so-called “Internet P2P” invites in Manyverse, and may have had a broken experience with them. Now with SSB CONN, there is also the possibility of reworking that part, and make it work more reliably.

The next months are going to be great for Manyverse. Now that I’m **working nearly full-time** on this, I can get a lot of stuff done that I’ve been wanting to finish. The second half of 2019 is looking fantastic for the project!

I’m also looking forward to collaborate with another developer – Luandro – who is forking Manyverse to build an **app dedicated to indigenous communities in Brazil**. I want to support him in that mission, and it anyway will be good if more than one person understands the source code. I’m also going to collaborate with **Dominic Tarr**, who’ll be staying at my place for a week in July, as part of his Europe-wide tour before he goes back to New Zealand. Hint: the “new kind of server” is on our agenda.

In case you missed it, a few weeks ago I spoke at MixitConf about big topics: Manyverse, the future internet, climate breakdown, degrowth, social media, etc. [Watch the talk here](https://mixitconf.org/2019/the-internet-in-2030). Sébastien, one of the conference organizers, invited me way back – on September 26th – to talk about Manyverse. Note: that was one day after the launch of the first version. I’ve been this whole time thinking and preparing this talk, and I think it’s worth taking 30 minutes to watch.

![Photograph of Andre Staltz holding a microphone while giving a talk; behind him you can see a vertical poster of the Mixit conference, and a part of the projected screen where slides are shown](../../images/201907conf.jpg)

Before I conclude this newsletter, let me remind you that if you find any bug whatsover in Manyverse, feel free to press **Email bug report** in the side menu. I actually read bug reports and try to categorize them. See you next month!