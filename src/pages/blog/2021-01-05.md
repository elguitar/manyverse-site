---
path: '/blog/2021-01-update'
date: '2021-01-05'
title: 'Jan 2021 update'
author: 'André Staltz'
authorUrl: 'https://staltz.com'
---

Hey backers,

I wish you a good start for 2021! As you'll read in this newsletter update, it will be a big year for Manyverse. You may not remember how the app was one year ago, but a lot was achieved in 2020: iOS launch, private messages, emoji reactions, translation to 9 languages, threads redesigned, audio messages, dark mode. On the team side, we welcomed new contributors, got 2 different grants, and we're constantly coordinating as a team. Imagine what the next 12 months will hold!

In December, Anders and I finally made the first production-ready version of [ssb-db2](https://github.com/ssb-ngi-pointer/ssb-db2) (the new database), getting it in shape to run inside Manyverse. This month's update is small, because the database work kept us really busy. Version 0.2101.5 ships some bug fixes and uses ssb-db2 for some parts of the app:

- ✅ Bug fix: Compose preview should not reset post
- ✅ Bug fix: glitches in the Thread screen's reply box
- ✅ Bug fix: tweak the style of the message header
- ✅ Bug fix: tweak the style of placeholder content
- 🔷 Use new database ssb-db2 for reactions
- 🔷 Improve German, Italian, and Spanish translations
- 🔷 Improve translations missing spaces on Welcome screen
- 🔷 Update backers in the Thanks screen

## New database

In the previous version, we included the new database just to migrate your data from one format to another. In this version, the new database is actually used by the user interface to display emoji reactions. This is a small change, and you won't notice much difference. In fact it's not yet faster than before, because we're still running both old database and new database at the same time.

The app should get a lot faster once we remove the old database and use only the new one. But for now, we're deploying gradually, to make sure things don't break all at once. I'm confident that for the next release, we will be using the new database for almost everything or literally everything.

Building a new database for SSB is a significant challenge, since the database is the central component in any SSB app, and traditionally has been handled by [many complex modules](https://github.com/flumedb) invented by Dominic Tarr. Anders and I were really busy (we made [36 PRs](https://github.com/ssb-ngi-pointer/ssb-db2/pulls?page=1&q=is%3Apr+is%3Aclosed+created%3A2020-12-01..2020-12-31) in December alone) building a faster replacement in just 3 months, and in December we fixed a ton of corner case bugs, while keeping an eye on performance.

## SSB Room 2.0

Many people use room servers as their daily method of fetching updates in Manyverse. You may remember that we [announced rooms on the Manyverse blog](https://www.manyver.se/blog/announcing-ssb-rooms/) in 2019. Although it's independent from Manyverse, I built it thinking about Manyverse, and it plays an important role nowadays.

For the NGI Pointer grant, we want to make the next generation of room servers, enabling some exciting new features, and enhancing privacy. In December, I also worked on designing rooms 2.0, and the new design can be [read as a document on GitHub](https://github.com/ssb-ngi-pointer/rooms2). If this subject interests you, I would highly appreciate some feedback in the form of GitHub issues! Our plan is to get started implementing the new design this year, and shipping support for it in Manyverse around mid 2021. So now is the time for feedback!

## Off-grid adoption

From the beginning, Manyverse has had the mission to be a truly off-grid social network, which means it is accessible and usable even offline. This is an important quality for communities that are typically offline or have an unfair (too expensive or too slow or both) internet connection. While I'm physically very far from such communities, I know people who are adjacent to these communities: Nicolás Pace (in our [team](https://manyver.se/team)), Luandro Vieira (has previously contributed code to Manyverse), and Ana Rosa from the socio-environmental project [Apenã](https://medium.com/apen%C3%A3). These people bring news!

Luandro has been staying at some indigenous settlements in Brazil, becoming a part of their communities, and helping them out as he can. [In this SSB post](https://viewer.scuttlebot.io/%257yBgT50oJ5kfKdXWf5WuxBlDOwtD520WIAlw9Z9LRYY%3D.sha256) he details how he helped the community get started with Manyverse! There's interest there, and potential, but so far he gathered learnings on what needs improvement before real adoption. Turns out, instructions in the local language are very important, not only on how to use Manyverse, but also the *Why*.

Also in Brazil, Ana Rosa and I have been remotely helping people in [a Quilombo settlement](https://www.meli-bees.org/the-women-who-pollinate-the-forest/) (settlements founded by people of African origin who were escaped slaves) to get started with Manyverse. We also gathered feedback on what needs improvement, but we were surprised they managed to get it working!

These two news personally meant a lot to me, the possibility for SSB on mobile to be a support for indigenous and black ancestry communities used to be just an idea, so hearing these stories come true boosts the feeling of duty and gives perspective to each new version we make. There's a lot we need to learn before they can properly benefit from Manyverse, and we also want to factor in these learnings into the UX grant. Onboarding needs to get better and more accessible.

## Coming up

Yesterday, Jacob and I had a meeting to discuss [Manyverse Desktop](https://gitlab.com/staltz/manyverse/-/issues/554). Jacob will be working on it regularly, and we believe it should be ready later this year! I'll keep the teaser short. :)

Have a great January,

— @andrestaltz