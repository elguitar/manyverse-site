---
path: '/blog/2020-07-update'
date: '2020-07-05'
title: 'July 2020 update'
author: 'André Staltz'
authorUrl: 'https://staltz.com'
---

Hey backers,

Manyverse keeps on growing! I worked 72 hours last month (35 of which were funded by you) on improving the app, but this newsletter should make it clear how a lot of different people were involved as well. The new version of Manyverse, **0.2007.3**, released yesterday, comes with a bunch of small but welcome fixes:

* 🇩🇪 Add German translation
* 🎉 Feature: shorten recovery phrase to only 24 words
* ✅ Bug fix: blobs purging settings should work
* ✅ Bug fix: correct heart emoji in the emoji picker
* ✅ Bug fix: margin between header and Thread posts
* ✅ (iOS) Fix tab bar height on iPhone X and later
* 🔷 Improve loading time of own avatar in several screens
* 🔷 Improve looks of private chat message bubbles
* 🔷 Improve the Recipients Input screen for private conversations
* 🔷 Improve typography in several screens
* 🔷 Update backers in Thanks screen
* Read more in the changelog for version [v0.2007.3](https://gitlab.com/staltz/manyverse/blob/master/CHANGELOG.md#020073-beta)

I'm proud that most of these changes were initiated by the community. The German translation is now usable (92% translated) thanks to our German speaking volunteers.

*Kas* from Denmark figured out that Manyverse was encoding the recovery words with unnecessarily too many words. Previously, your account's identity was made of 48 words. It turns out, half of those words were redundant! I missed a detail in the encoding of the cryptographic keys, but this is now gladly fixed. Your account's identity is now made of only 24 words. There is nothing new you need to do because **the new 24-word recovery phrase is simply the first half of your 48-word recovery phrase**. It should be significantly easier to remember or write down on paper. But Manyverse will also accept your old 48-word recovery phrase, if you wish.

*David Gómez* helped out with several UI tweaks, specially on iOS. I myself worked on fixing the automatic blobs deletion system, it should work correctly now. You can try it out in the Settings screen. I also worked on other UI improvements, such as when starting a new private message, and speeding up the UI performance in some corners.

Last month one thing that has kept me busy was beyond code. **Manyverse is gradually becoming a team effort**, not just my effort, and I've been coordinating with people more and more. In the past, *Jacob Karlsson* and I have gotten together in Jitsi calls. This time, we expanded a bit further, and invited David and Nicolás too, for a first group call!

![Screenshot of a video call in Jitsi, showing four participants, David, Nico, Jacob, and Andre, some of which are laughing or smiling](../../images/jitsi-screenshot-meeting01.jpg)

We didn't have any other agenda other than getting to know each other and understanding what the word "team" means to us. In an open source project, contributors often come and go. They may stay for a very long time, or they may contribute for just a few weeks. This is natural because contributions are voluntary. Although we'd like to eventually have funds for all team members, we're different from (for example) a company, because open source projects always have this voluntary and open-to-join aspect. Now we also have a [Team page](/team) on our website, displaying currently active contributors!

Still about team, recently I received an email from a UX designer, *Wouter Moraal*, who was very interested in working on Manyverse. We exchanged a lot of lengthy messages, and it turns out, we decided to apply to some grants together! If it passes, Wouter and I are going to work in close cooperation to improve the user experience in the app, starting later this year. Specifically, we're focused on **solving the onboarding user experience**, including the invitation workflow and initial sync performance. This is arguably the biggest problem to solve in the SSB ecosystem, and most solutions we discover for Manyverse should be applicable for other SSB apps, so we're very excited about it! Wish us luck!

Have a sunny new week!

— @andrestaltz