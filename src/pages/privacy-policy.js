import React from 'react'
import {graphql} from 'gatsby'
import Layout from '../components/layout'
import SEO from '../components/seo'
import styles from '../templates/blog.module.css'

export const query = graphql`
  query {
    markdownRemark(fileAbsolutePath: {regex: "/wiki\/Privacy-Policy.md$/"}) {
      fileAbsolutePath
      html
    }
  }
`

export default function PrivacyPolicy({data}) {
  const {markdownRemark} = data
  const {html} = markdownRemark
  const title = 'Privacy Policy'
  return (
    <Layout>
      <SEO title={title} />
      <div className={styles.blogContainer}>
        <h1>{title}</h1>
        <div
          className={styles.blogContent}
          dangerouslySetInnerHTML={{__html: html}}
        />
      </div>
    </Layout>
  )
}
