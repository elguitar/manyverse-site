---
date: 'connections05'
path: "/faq/connect-pub"
title: "How do I connect to Pub servers?"
---

Pub servers are hubs of data that a community uses to communicate indirectly. The pub holds a copy of each member's social data, and when any member gets online, they can fetch the latest pieces of data from the pub server. This is good because friends don't need to be online at the same time in order to synchronize their data, but instead they use the pub to upload data, and the pub stays always online. On the other hand, pubs can sometimes contain a large amount of data, which can take a long time to download.

**First make sure you have internet connection.** In Manyverse, go to the tab on the right with a globe icon, to see the *Connections* tab. If the "computer connected to a cable" icon is highlighted in blue, then it means you have internet connection and can use Pub invites. If it's not blue yet, wait a few seconds.

**You need an invite code from a pub**, and there is no global way of finding these pubs. Instead, you need to *be invited* by the person(s) who operates the pub server. Remember, pubs are for a specific community, so by accepting an invite, you are joining that community. Once you have an invite code, then **tap the green circular button** in the *Connections* screen, and choose "**Paste invite**". A new screen will open, and you should paste the invite code you received for the pub server. It should look like a non-readable sequence of characters. After confirming, the *Connections* screen will show the pub server being connected with a yellow dot, and successful connection will display a green dot.

Keep in mind that depending on the specific pub server you just joined, your Manyverse app might download a lot of social data, potentially over 500 MB, and the overall joining process may take **several minutes**.
