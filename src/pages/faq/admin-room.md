---
date: 'connections08'
path: "/faq/admin-room"
title: "How do I start my own Room server?"
---

So you want to host a room server, that's great! You will have a fundamental role in enabling people to use the social network.

As a room server administrator, you will pay for its hosting online, and maintain it in case of downtime or other technical problems.

We *highly recommend* to use [go-ssb-room](https://github.com/ssb-ngi-pointer/go-ssb-room), the best implementation of rooms so far. There also exists [ssb-room](https://github.com/staltz/ssb-room), but it is outdated and has less features. Check the video below about `go-ssb-room`:

<iframe width="560" height="315" src="https://www.youtube.com/embed/W5p0y_MWwDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

You can host the server on a device you own, such as a Raspberry Pi, or on some cloud provider ("VPS"). If you choose to host it on a device of your own, we suggest [Yunohost](https://yunohost.org) because it already contains `go-ssb-room`, easily installable with the press of one button.

If you have difficulties with getting started, feel free to [open an issue on GitHub](https://github.com/ssb-ngi-pointer/go-ssb-room/issues/new).
