---
date: 'connections07'
path: "/faq/admin-pub"
title: "How do I start my own Pub server?"
---

Each Pub server has its own administrator, a person who started the server, who pays for its hosting bills, and maintains it in case of downtime or other technical problems. If you are interested in starting your own SSB pub, then we recommend this [GitHub repository](https://github.com/ahdinosaur/ssb-pub) which provides easy guides as well as detailed instructions.
