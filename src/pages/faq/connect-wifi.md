---
date: 'connections03'
path: "/faq/connect-wifi"
title: "How do I use Wi-Fi to connect with people?"
---

Wi-Fi connections work between Manyverse and other apps such as Patchwork and Patchbay. The idea is that friends join the same Wi-Fi hotspot and then they will see each other inside the app.

**First turn on Wi-Fi on your device**. Open the *Settings* on your Android phone, and look for the menu *Connections*, and then select *Wi-Fi*. Choose a Wi-Fi network to join. Make sure your friend also joins the same Wi-Fi network.

Then, **open Manyverse** and then once you are on the screen that says *Messages*, go to the tab on the right with a globe icon, to see the *Connections* tab. The Wi-Fi icon should be highlighted blue, meaning it's enabled. If it's not blue yet, wait a few seconds.

There is nothing else you need to do, because your friends in the same Wi-Fi will be displayed in the *Connections* tab as soon as possible. They might show with their [SSB](https://scuttlebutt.nz) ID code, which looks like `@WOchhgXspSh0iyT+pj...`. You can find your own SSB ID on the left-side menu in the app. Before choosing a friend to connect to, make sure that their SSB ID is correct. Ask them what their SSB ID is. Then, just **tap** your friend's list item to connect with them.

- Choose **Connect and follow** to create a Wi-Fi link with them AND follow their Manyverse profile
  - This is useful to fetch their data, you should choose this option when the other person is your friend
- Choose **Connect** to ONLY create a Wi-Fi link without following them
  - This is useful when the other person is an acquaintance and you only want to fetch updates from their social circles but not from this person

