---
date: 'connections02'
path: "/faq/connect-bluetooth"
title: "How do I use Bluetooth to connect with people?"
---

Bluetooth connections work only with other phones nearby, usually a few meters close. The farther away the phones are, the more difficult the connections are established. Also, the more other Bluetooth devices there are around (such as loud speaker or mouse or headphones), the harder it is to connect in Manyverse, so make sure you find a good isolated corner. So far, Bluetooth connections only work between Manyverse and Manyverse, not with other apps.

Once you find a friend whom you wish to connect with over Bluetooth, **both of you should follow the instructions below**, and preferably with the phones less than 1 meter apart.

First make sure your phone supports Bluetooth. This is true for most devices, but to be sure, open the *Settings* on your Android phone, and look for the menu *Connections*, and then *Bluetooth*. **Turn on Bluetooth in the Android settings.**

Then, **open Manyverse** and then once you are on the screen that says *Messages*, go to the tab on the right with a globe icon, to see the *Connections* tab. The Bluetooth icon should be highlighted blue, meaning it's enabled. If it's not blue yet, wait a few seconds.

On this *Connections* tab, there is a **green circular button** at the bottom, tap that to open a small menu, and then choose **Bluetooth seek**. The other phone should do this too, at the same time.

A dialog will open asking for your permission to scan Bluetooth, this is normal and you should give it permission. Then, **wait for your friend's phone to appear** in the *Connections* tab. The Bluetooth icon in this screen will show a loading spinner indicator whenever it is scanning for nearby friends. If this spinner does not show anymore, then repeat the operation of pressing **Bluetooth seek** in the green button's menu. Remember, your friend needs to follow these same instructions at the same time as you.

Once your friend appears in the *Connections* tab, their name might show as the *name of the device* (e.g. Samsung Galaxy) not the name of their profile in Manyverse. Make sure that device name is actually from your friend's device. Then, just **tap** your friend's list item to connect with them.

- Choose **Connect and follow** to create a Bluetooth link AND follow their Manyverse profile
  - This is useful to fetch their data, you should choose this option when the other person is your friend
- Choose **Connect** to ONLY create a Bluetooth link without following them
  - This is useful when the other person is an acquaintance and you only want to fetch updates from their social circles but not from this person
