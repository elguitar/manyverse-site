---
date: 'account04'
path: "/faq/manyverse-folder-location"
title: "Where is the Manyverse folder located?"
---

**On Android**, it's only accessible by the Manyverse app,
- `/data/data/se.manyver/files/.ssb`

**On iOS**, it's an iOS-provided directory that only Manyverse can access.

**On Windows**, it's either:
- `C:\Documents and Settings\YOURUSERNAME\Application Data\manyverse`
- or `C:\Users\YOURUSERNAME\AppData\Roaming\manyverse`
- or `C:\ProgramData\manyverse`

**On macOS**,
- `~/Library/Application Support/manyverse`

**On Linux**,
- `~/.config/manyverse`