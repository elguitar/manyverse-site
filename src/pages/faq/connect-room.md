---
date: 'connections06'
path: "/faq/connect-room"
title: "How do I connect to Room servers?"
---

Room servers are an alternative to [Pubs](/faq/connect-pub) with important differences: rooms do not store any user data, but instead allow currently online people to connect to each other and synchronize their feeds. Rooms are only useful when there are other people online connected to it, and are an easy way of discovering new people without downloading a lot of data at once. Each room server represents a common interest, much like [IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat) rooms. Read more about [Rooms here](/blog/announcing-ssb-rooms).

**First make sure you have internet connection.** In Manyverse, go to the tab on the right with a globe icon, to see the *Connections* tab. If the "computer connected to a cable" icon is highlighted in blue, then it means you have internet connection and can use Room invites. If it's not blue yet, wait a few seconds.

**You need an invite code from a room**, and there is no global way of finding these rooms. Instead, you need to *be invited* by the person(s) who operates the room server, or someone who is already a member of the room. Remember, rooms are for a community of some common interest, so by accepting an invite, you are joining that community. Once you have an invite code, then **tap the green circular button** in the *Connections* screen, and choose "**Paste invite**". A new screen will open, and you should paste the invite code you received for the room server. It should look like a non-readable sequence of characters. After confirming, the *Connections* screen will show the room server being connected with a yellow dot, and successful connection will display a green dot.

The connected room indicator will also show who else is online in that room, and then you can choose to connect to those persons one by one. The strangers online will show only by their [SSB](https://scuttlebutt.nz) ID.
