---
date: 'account01'
path: "/faq/account-on-many-devices"
title: "How can I use my account on many devices?"
---

**Sorry, you cannot.** This is a side effect of your account living locally on the device, so that there is no "account in the cloud" to log into. Some people misuse the 24-word recovery feature to have the same account authoring messages on several devices, but this is risky: it can "fork" your account and cause it to be **irrecoverably stuck**. The recovery from 24 words is only intended for bringing back your account once it has been lost (e.g. you uninstalled the app, or the device got lost).

You have to create **a separate account for each device**. In the future, we plan to have an easy feature to automatically "bind" all of your accounts together, so that it will seem like one account.
