---
date: 'connections01'
path: "/faq/connections"
title: "How can I connect with people?"
---

Once you open Manyverse, you might notice there is no one to interact with, and there is no search field where to look for other people. This is unusual for a social network app, but because it is [off grid](/faq/off-the-grid) (it's not on the internet, it's spread out on thousands of different phones), your app starts empty and assumes nothing. It's up to you to choose how to find other phones that have Manyverse installed. The social network emerges when people chip in with a slice of their social data, sharing it with people they want to be friends with.

There are two different styles of connecting with people: **inviting persons** one at a time, or **joining communities**.

## Invite a friend

To bring one friend at a time to your Manyverse social circles, you can use either Bluetooth, Wi-Fi, or Internet P2P invites. Click these links to learn more how to use these connection modes:

- [How do I use Bluetooth to connect with people?](/faq/connect-bluetooth)
- [How do I use Wi-Fi to connect with people?](/faq/connect-wifi)
- [How do I use "Internet P2P invites"?](/faq/connect-dht-invites)

## Join a community

To join a [SSB](https://scuttlebutt.nz) community, you need to connect to a server on the internet, that is either a [Pub server](https://www.scuttlebutt.nz/concepts/pub) or a [Room server](/blog/announcing-ssb-rooms). These servers are owned by community members, and it's wise to be aware of who exactly is the administrator of a server, because once your app connects to that, it may leak some metadata about who you are. Prefer to connect only to servers that you trust. Click these links to learn how to connect to these servers:

- [How do I connect to Pub servers?](/faq/connect-pub)
- [How do I connect to Room servers?](/faq/connect-room)

You can also start your own server for your community. Follow these links for more instructions:

- [How do I start my own Pub server?](/faq/admin-pub)
- [How do I start my own Room server?](/faq/admin-room)
