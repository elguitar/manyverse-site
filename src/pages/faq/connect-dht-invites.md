---
date: 'connections04'
path: "/faq/connect-dht-invites"
title: "How do I use \"Internet P2P invites\"?"
---

**NOTE: P2P Invites are no longer supported in Manyverse because we noticed they did not reliably work. The instructions on this page do not apply on recent versions of Manyverse.**

If your friend is not nearby, but both you and them have internet access, then you can connect directly with each other — without intermediary servers — using *Internet P2P* invites, also known as DHT (Distributed Hash Table) invites. This method sometimes does not work, depending on restrictions imposed by your mobile carrier, and your friend's mobile carrier.

**First make sure you have internet connection.** In Manyverse, go to the tab on the right with a globe icon, to see the *Connections* tab. If the "person connected to a cable" icon is highlighted in blue, then it means you have internet connection and can use Internet P2P invites. If it's not blue yet, wait a few seconds.

**If you received an invite code** from your friend, then **tap the green circular button** in the *Connections* screen, and choose "**Paste invite**". A new screen will open, and you should paste the invite code you received from your friend. It should look like a non-readable sequence of characters, such as `dht:f8Cia/1i+c...`. If the code starts with a text such as "Connect with me on Manyverse by pasting this invite code", then make sure to delete that part until there is only the code that starts with `dht:`. Once you confirm, the *Connections* screen will show a light-blue list item indicating that the app is "looking for your friend online". **This loading process can take up to 2 minutes**, and it only works if your friend is **currently online** on Manyverse, because the phones connect directly to each other over the internet. Send an email or another instant message to your friend to coordinate the right time to connect with each other.

**If you want to create and send an invite code** to your friend, then **tap the green circular button** in the *Connections*, and choose "**Create invite**". The app will generate an invite code, which you can copy and send to your friend, or press the "Share" button and send it via some other app on your phone. After that, the *Connections* screen will show a light-blue list item indicating that the app is "waiting for your friend" to come online. This is a placeholder until your friend claims the invite code in Manyverse. Make sure that both of you are online in Manyverse at the same time when they claim your invite code.