import React from 'react'
import {Link} from 'gatsby'
import styles from './footer.module.css'

const Footer = () => (
  <footer className={styles.footer}>
    <div className={styles.content}>
      <ul>
        <li>
          <h1>More info</h1>
        </li>
        <li>
          <Link to="/faq">FAQ</Link>
        </li>
        <li>
          <Link to="/blog">Blog</Link>
        </li>
        <li>
          <Link to="/roadmap">Roadmap</Link>
        </li>
        <li>
          <Link to="/privacy-policy">Privacy Policy</Link>
        </li>
      </ul>
      <ul>
        <h1>Contact</h1>
        <li>
          <Link to="/donate">Donate</Link>
        </li>
        <li>
          <Link to="/team">Team</Link>
        </li>
        <li>
          <a href="mailto:contact@staltz.com">Email</a>
        </li>
      </ul>
      <ul>
        <li>
          <h1>Community</h1>
        </li>
        <li>
          <a href="https://gitlab.com/staltz/manyverse">GitLab</a>
        </li>
        <li>
          <Link to="/translations">Translations</Link>
        </li>
        <li>
          <a href="https://twitter.com/manyver_se">Twitter</a>
        </li>
        <li>
          <a href="https://fosstodon.org/@manyver_se/">Mastodon</a>
        </li>
      </ul>
      <ul>
        <li>
          <h1>Motivation</h1>
        </li>
        <li>
          <a href="https://www.youtube.com/watch?v=UjfWAbGfPh0">TEDxGeneva</a>
        </li>
        <li>
          <a href="https://www.youtube.com/watch?v=8GE5C9-RUpg">
            Full Stack Fest
          </a>
        </li>
        <li>
          <Link to="/ux-research">UX Research</Link>
        </li>
      </ul>
    </div>
    <p>
      The <a href="https://gitlab.com/staltz/manyverse">Manyverse app</a> is
      open source software licensed as{' '}
      <a href="https://www.mozilla.org/en-US/MPL/">
        Mozilla Public License 2.0
      </a>{' '}
      and copyright by{' '}
      <a href="https://gitlab.com/staltz/manyverse/blob/master/AUTHORS">
        The Manyverse Authors
      </a>
      . The{' '}
      <a href="https://gitlab.com/staltz/manyverse-site">manyver.se website</a>{' '}
      is licensed as{' '}
      <a href="https://creativecommons.org/licenses/by-sa/4.0/">
        Creative Commons BY-SA 4.0
      </a>
      , copyright by <a href="https://staltz.com">Andre Staltz</a>.
    </p>
  </footer>
)

export default Footer
