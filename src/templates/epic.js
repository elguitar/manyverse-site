import React from 'react'
import {graphql, Link} from 'gatsby'
import Img from 'gatsby-image'
import visit from 'unist-util-visit'
import findAfter from 'unist-util-find-after'
import unified from 'unified'
import remarkParse from 'remark-parse'
import remarkHtml from 'remark-html'
import Layout from '../components/layout'
import SEO from '../components/seo'
import styles from './epic.module.css'

export const pageQuery = graphql`
  query($issue: String!) {
    gitlabIssue(iid: {eq: $issue}) {
      title
      iid
      labels {
        name
        description
        text_color
        color
      }
      description
      webUrl
      assignees {
        username
      }
    }

    allAuthorsCsv {
      edges {
        node {
          name
          picture
          gitlab
        }
      }
    }

    allImageSharp {
      edges {
        node {
          fluid {
            src
            aspectRatio
            sizes
            srcSet
            originalName
          }
        }
      }
    }
  }
`

function fixGitlabImages() {
  return ast => {
    visit(ast, 'image', image => {
      if (
        image.url &&
        typeof image.url === 'string' &&
        image.url.startsWith('/uploads')
      ) {
        image.url = `https://gitlab.com/staltz/manyverse${image.url}`
      }
      return image
    })
    return ast
  }
}

function ignoreAfterDoubleHorizontalLine() {
  return ast => {
    const hr1 = findAfter(ast, 0, 'thematicBreak')
    if (!hr1) return ast
    const hr2 = findAfter(ast, hr1, 'thematicBreak')
    if (!hr2) return ast
    hr1.type = 'text'
    hr1.value = ''
    hr2.type = 'text'
    hr2.value = ''
    let foundHR2 = false
    visit(ast, null, node => {
      if (foundHR2) {
        if (node.children && node.children.length > 0) node.children.length = 0
        if (node.value) node.value = null
        if (node.type === 'thematicBreak') {
          node.type = 'text'
          node.value = ''
        }
      } else if (node === hr2) {
        foundHR2 = true
      }
    })
    return ast
  }
}

function Assignee(props) {
  const {name, picture} = props
  const SIZE = 48
  const width = `${SIZE}px`
  const height = `${SIZE}px`
  const borderRadius = `${Math.ceil(SIZE * 0.5)}px`
  return (
    <div className={styles.assignee}>
      <Img
        fluid={picture}
        style={{width, height, borderRadius, marginRight: '10px'}}
      />
      <Link to={'/team'} className={styles.assigneeName}>
        {name}
      </Link>
    </div>
  )
}

export default function Epic({data}) {
  const {gitlabIssue, allAuthorsCsv, allImageSharp} = data
  const {iid, labels, title, description, webUrl, assignees} = gitlabIssue
  const pictures = allImageSharp.edges.map(({node}) => node.fluid)

  for (const assignee of assignees) {
    const author = allAuthorsCsv.edges.find(
      ({node: author}) => author.gitlab === assignee.username
    ).node
    assignee.name = author.name
    const pictureFluid = pictures.find(p => p.originalName === author.picture)
    if (pictureFluid) {
      assignee.picture = pictureFluid
    }
  }

  const html = String(
    unified()
      .use(remarkParse)
      .use(ignoreAfterDoubleHorizontalLine)
      .use(fixGitlabImages)
      .use(remarkHtml)
      .processSync(description)
  )

  const label = labels.find(label => label.name.startsWith('phase::'))
  const labelName = label.description.split('(')[0].trim()
  const labelBackgroundColor = label.color
  const labelColor = label.text_color
  const labelIsDone =
    label.name === 'phase::complete' || label.name === 'phase::post-mortem'

  return (
    <Layout>
      <SEO title={title} />
      <div className={styles.container}>
        {labelIsDone ? (
          <Link to={'/features'}>← Back to Features</Link>
        ) : (
          <Link to={'/roadmap'}>← Back to Roadmap</Link>
        )}
        <h1 className={styles.title}>{title}</h1>
        {labelIsDone ? null : (
          <div
            className={styles.label}
            style={{
              backgroundColor: labelBackgroundColor,
              color: labelColor,
            }}
          >
            Phase: {labelName}
          </div>
        )}
        {assignees.map(assignee => (
          <Assignee key={assignee.username} {...assignee} />
        ))}
        <div
          className={styles.content}
          dangerouslySetInnerHTML={{__html: html}}
        />
        <p className={styles.issueLink}>
          <a href={webUrl}>More information in the GitLab issue #{iid}</a>
        </p>
      </div>
    </Layout>
  )
}
