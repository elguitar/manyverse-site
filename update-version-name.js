const fs = require('fs')
const axios = require('axios').default

axios
  .get('https://gitlab.com/staltz/manyverse/-/raw/master/package.json')
  .then(function(response) {
    fs.writeFileSync(
      './static/latestversion.json',
      JSON.stringify(response.data.version)
    )
  })
