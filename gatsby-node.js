const path = require('path')

const createBlogPages = async ({actions, graphql}) => {
  const {createPage} = actions

  const result = await graphql(`
    {
      allMarkdownRemark(
        sort: {order: DESC, fields: [frontmatter___date]}
        limit: 1000
      ) {
        edges {
          node {
            fileAbsolutePath
            frontmatter {
              path
            }
          }
        }
      }
    }
  `)

  if (result.errors) throw result.errors

  for (let edge of result.data.allMarkdownRemark.edges) {
    if (edge.node.fileAbsolutePath.includes('/src/pages/blog/')) {
      createPage({
        path: edge.node.frontmatter.path,
        component: path.resolve(`src/templates/blog.js`),
        context: {},
      })
    }
  }
}

const createFAQPages = async ({actions, graphql}) => {
  const {createPage} = actions

  const result = await graphql(`
    {
      allMarkdownRemark(
        sort: {order: DESC, fields: [frontmatter___date]}
        limit: 1000
      ) {
        edges {
          node {
            fileAbsolutePath
            frontmatter {
              path
            }
          }
        }
      }
    }
  `)

  if (result.errors) throw result.errors

  for (let edge of result.data.allMarkdownRemark.edges)
    if (edge.node.fileAbsolutePath.includes('/src/pages/faq/')) {
      createPage({
        path: edge.node.frontmatter.path,
        component: path.resolve(`src/templates/faq.js`),
        context: {},
      })
    }
}

const createMilestonePages = async ({actions, graphql}) => {
  const {createPage} = actions

  const result = await graphql(`
    {
      allGitlabIssue {
        edges {
          node {
            iid
          }
        }
      }
    }
  `)

  if (result.errors) throw result.errors

  for (let edge of result.data.allGitlabIssue.edges) {
    createPage({
      path: '/epic/' + edge.node.iid,
      component: path.resolve(`src/templates/epic.js`),
      context: {
        issue: edge.node.iid,
      },
    })
  }
}

exports.createPages = async ({actions, graphql}) => {
  actions.createRedirect({
    fromPath: '/latestversion',
    toPath: '/latestversion.json',
    isPermanent: true,
  })
  await createBlogPages({actions, graphql})
  await createFAQPages({actions, graphql})
  await createMilestonePages({actions, graphql})
}
